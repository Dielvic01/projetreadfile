menu = {}
menu['1']="Ajouter Etudiant."
menu['2']="Supprimer Etudiant."
menu['3']="Chercher Etudiant"
menu['4']="Quitter"

liste = []
def ajouter_etudiant():
    dico = {}
    nom = input("nom: ")
    prenom = input("prenom: ")
    age = int(input("age: "))
    dico['nom']=nom
    dico['prenom']=prenom
    dico['age']=age
    return liste.append(dico)

def supprimer_etudiant():
    s = input('rentrez le nom de l\'étudiant à supprimer: ')
    [liste.remove(d) for d in liste if d['nom'] == s]
    print(liste)

def chercher_etudiant():
    c = input('rentrez le nom de l\'étudiant que vous recherchez: ')
    r =[d for d in liste if d['nom'] == c]
    print(*r)


while True:
  try:
      choix = int(input(f'Selectionnez le menu {menu} : '))
      if choix == 1:
          ajouter_etudiant()
      elif choix == 2:
          supprimer_etudiant()
      elif choix == 3:
          chercher_etudiant()
      elif choix == 4:
          break
      else:
          print('Saisissez une valeur comprise dans le menu')
  except Exception:
    continue
